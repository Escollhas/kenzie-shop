import styled from "styled-components";

export const CartBooks = styled.div`
  width: 250px;
  text-transform: uppercase;
  display: flex;
  flex-flow: column wrap;
  justify-content: space-between;
  border: 3px solid black;
  border-radius: 11px;
  background-color: #b5b5b5;
  margin: 3px;

  img {
    width: 188px;
    height: 272px;
    margin: 0 auto;
    border-radius: 14px;
    margin-bottom: 3px;
    margin-top: 3px;
  }
  span {
    text-transform: none;
    font-weight: bolder;
    font-size: 1.4rem;
  }
  button {
    padding: 8px;
    text-transform: uppercase;
    font-weight: bolder;
    border-radius: 13px;
    margin-top: 3px;
    cursor: pointer;
  }
  div {
    padding-left: 25px;
  }
`;
