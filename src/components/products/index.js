import { useSelector } from "react-redux";
import Product from "../product";
import { ContentMain } from "./styles";
import { useEffect, useStyles } from "react";
const Products = () => {
  const products = useSelector((state) => state.products);

  return (
    <ContentMain>
      <h1>Catalogue Books:</h1>
      {products.map((product, index) => (
        <Product key={index} product={product} />
      ))}
    </ContentMain>
  );
};

export default Products;
