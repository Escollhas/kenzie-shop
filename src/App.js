import "./main.css";
import Header from "./components/header";
import Routes from "./routes";
function App() {
  return (
    <div className="main">
      <Header />
      <Routes />
    </div>
  );
}

export default App;
