import { useSelector } from "react-redux";
import { ContentMain } from "../products/styles";
import ShowCart from "./showCart";

const Cart = () => {
  const cart = useSelector((state) => state.cart);
  const local = JSON.parse(localStorage.getItem("token")) || [];

  console.log("OI");
  console.log(local);
  return (
    <ContentMain>
      <h1>Cart Books: </h1>
      {local.map((value, index) => (
        <ShowCart key={index} product={value} />
      ))}
    </ContentMain>
  );
};

export default Cart;
