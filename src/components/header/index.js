import { Link } from "react-router-dom";
import { ShowHeader } from "./styles";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
const Header = () => {
  //console.log(useSelector((store) => store));
  const cartPrice = useSelector((store) => store.cart);
  const localPrice = JSON.parse(localStorage.getItem("token")) || [];

  const [total, setTotal] = useState(0);
  useEffect(() => {
  

    setTotal(localPrice.reduce((value, index) => value + index.price, 0));
  }, [localPrice]);
  return (
    <ShowHeader>
      <div>
        <Link to="/">Kenzie Shop</Link>
      </div>

      <div>
        <Link to="/">Books</Link>
        <Link to="carrinho">Cart</Link>
        <span style={{ color: "blue" }}> R$:{total}</span>
      </div>
    </ShowHeader>
  );
};

export default Header;
