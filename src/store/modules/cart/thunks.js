import { addToCart } from "./actions";

export const addToCartThunk = (product) => {
  return async (dispatch, getState) => {
    const list = JSON.parse(localStorage.getItem("token")) || [];
    list.push(product);
    localStorage.setItem("token", JSON.stringify(list));
    dispatch(addToCart(product));
  };
};
