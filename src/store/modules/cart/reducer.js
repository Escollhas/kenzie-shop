const cartReducer = (state = [], action) => {
  switch (action.type) {
    case "@cart/ADD":
      const product = action.product;
      return [...state, product];
    default:
      return state;
  }
};

export default cartReducer;
