import { Cart } from "./styles";

const ShowCart = ({ product }) => {
  return (
    <Cart>
      <img src={product.img} alt="book"></img>
      <div>Produto: {product.name}</div>
      <div>Price: {product.price}</div>
    </Cart>
  );
};

export default ShowCart;
