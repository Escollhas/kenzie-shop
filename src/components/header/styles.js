import styled from "styled-components";

export const ShowHeader = styled.div`
  color: yellow;
  height: 90px;
  display: flex;
  flex-flow: row wrap;
  color: #483c3c;
  font-size: 2rem;
  font-style: italic;
  border-bottom: 3px solid #746d6d;
  box-shadow: 3px 0px 11px #383443;
  text-align: center;
  div {
    width: 50%;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-around;
    align-items: center;
  }
  div:first-child {
    box-shadow: 1px 0px 7px #746d6d;
  }
  div a {
    text-decoration: none;
    color: black;
  }
  div a:active {
    color: inherit;
  }
`;
