const defaultState = [
  {
    id: 1,
    name: "A Court of Thorns and Roses",
    price: 10,
    img: "https://m.media-amazon.com/images/I/51RlqfADKXL.jpg",
  },
  {
    id: 2,
    name: "Children of Ash and Elm: A History of the Vikings",
    price: 20,
    img:
      "https://images-na.ssl-images-amazon.com/images/I/41-DsmA0iHL._SX322_BO1,204,203,200_.jpg",
  },
  {
    id: 3,
    name: "A Cat's Tale: A Journey Through Feline History",
    price: 23000,
    img:
      "https://images-na.ssl-images-amazon.com/images/I/51YdmOOwDuL._SX358_BO1,204,203,200_.jpg",
  },
];

const productsReducer = (state = defaultState) => state;

export default productsReducer;
