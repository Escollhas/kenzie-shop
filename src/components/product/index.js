import { useDispatch } from "react-redux";
import { addToCartThunk } from "../../store/modules/cart/thunks";
import { CartBooks } from "./styles";
const Product = ({ product }) => {
  const dispatch = useDispatch();
  return (
    <CartBooks>
      <img src={product.img} alt="book"></img>
      <div>
        Name:<span>{product.name}</span>
      </div>
      <div>
        Book Id: <span> {product.id}</span>
      </div>
      <div>
        Price: <span> USD${product.price}</span>
      </div>
      <button onClick={() => dispatch(addToCartThunk(product))}>
        Add to Cart
      </button>
    </CartBooks>
  );
};

export default Product;
