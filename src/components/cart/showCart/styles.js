import styled from "styled-components";
import { CartBooks } from "../../product/styles";

export const Cart = styled(CartBooks)`
  font-size: 1.1rem;
  text-transform: none;
  width: 230px;
  img {
    width: 158px;
    height: 252px;
  }
  div {
    padding: 5px;
    padding-left: 35px;
  }
`;
