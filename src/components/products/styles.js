import styled from "styled-components";
export const ContentMain = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  h1 {
    width: 100%;
    text-align: center;
  }
`;
